# crabsecco

[![maintenance-status: passively-maintained](https://img.shields.io/badge/maintenance--status-passively--maintained-forestgreen)](https://gist.github.com/rusty-snake/574a91f1df9f97ec77ca308d6d731e29)

crabjail seccomp compiler.

## Usage

Basic usage:

    crabsecco path/to/crabsecco-filter.toml

With output-format:

    crabsecco --output-format=bpf,pfc path/to/crabsecco-filter.toml

Without installation:

    cargo run --release -- path/to/crabsecco-filter.toml

## Examples

Build a minimal sandbox with the help of [bubblewrap](https://github.com/containers/bubblewrap)

    cargo run --release -- examples/flatpak.toml
    bwrap_args=(
        --die-with-parent
        --new-session
        --cap-drop all
        --seccomp 3
        --unshare-all
        # Remove this if you do not need any network access.
        --share-net
        --proc /proc
        --ro-bind-try /sys /sys
        --dev /dev
        --dev-bind-try /dev/dri /dev/dri
        --ro-bind /etc /etc
        --ro-bind /usr /usr
        # If your system does not have a unified file-system hierarchy
        # comment the --symlinks below and uncomment these --ro-binds
        #--ro-bind /bin /bin
        #--ro-bind /lib /lib
        #--ro-bind /lib64 /lib64
        #--ro-bind /sbin /sbin
        --symlink /usr/bin /bin
        --symlink /usr/lib /lib
        --symlink /usr/lib64 /lib64
        --symlink /usr/sbin /sbin
        --dir /tmp
        --dir /var/tmp
        --ro-bind /run /run
        --bind "$XDG_RUNTIME_DIR" "$XDG_RUNTIME_DIR"
        --bind "$HOME" "$HOME"
    )
    bwrap "${bwrap_args[@]}" 3<./flatpak.bpf /usr/bin/gedit


## Installation

### Dependencies

- libseccomp
- cc

### Prerequisites

- [Rust](https://www.rust-lang.org/) >= 1.60.0
- libseccomp development files

**Fedora Linux:**

    dnf install cargo libseccomp-devel

### Build

    cargo build --release

### Install

    install -Dm755 target/release/crabsecco /usr/local/bin/crabsecco

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## Related projects

In alphabetical order:

- [kafel](https://github.com/google/kafel)
- [seccomp-tools](https://github.com/david942j/seccomp-tools)

## FAQ

## Links

Repository: https://codeberg.org/crabjail/crabsecco

Bug-Tracker: https://codeberg.org/crabjail/crabsecco/issues

## Changelog

See [CHANGELOG.md](CHANGELOG.md) for the full changelog.

## Authors and Contributors

See [AUTHORS](AUTHORS)

## License

GPL-3.0-or-later, see [COPYING](COPYING) for details.
