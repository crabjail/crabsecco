crabsecco manpage
=================

Build and Install
-----------------

Fedora Linux:

```bash
dnf install ninja-build python3-docutils python3-pygments
ninja
install -Dm644 crabsecco.1 /usr/local/share/man/man1/crabsecco.1
install -Dm644 crabsecco.1.html /usr/local/share/doc/crabsecco/crabsecco.1.html
install -Dm644 crabsecco-filter.5 /usr/local/share/man/man5/crabsecco-filter.5
install -Dm644 crabsecco-filter.5.html /usr/local/share/doc/crabsecco/crabsecco-filter.5.html
```
