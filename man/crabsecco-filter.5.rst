################
crabsecco-filter
################

crabsecco-filter syntax
#######################

:Version: 0.1.0
:Manual section: 5

DESCRIPTION
===========

A crabsecco-filter is a toml file consisting of a HEAD_, one or more RULES_ and
optional ASSERTIONS_.

HEAD
====

name
  The name of the filter.

description
  A description of the filter.

btree-optimization
  Enable binary tree optimization. See ``SCMP_FLTATR_CTL_OPTIMIZE`` in
  ``seccomp_attr_set(3)`` for details.

default-action
  The default action of the filter. See ``seccomp_init(3)`` for details.
  
  This action is used if no rule is set for a syscall.
  
  Due to limitations of the libseccomp API it is not possible to create a rule
  with this default-action as action.

architecture
  The name of the architecture this filter is written for.
  
  Compatibility note: The syntax and semantic of this key will likely change in
  the future.

RULES
=====

action
  The action to be taken if this rule matches a syscall.
  
  This action must be different from ``default-action``.
  
  Supported actions:
  
   * ``Allow``
   * ``Deny`` or ``EPERM``
   * ``Hide`` or ``ENOSYS``
   * ``EAFNOSUPPORT``
   * ``kill`` to kill the thread which called this syscall.

syscall
  The name of the syscall this rule should apply.

comperators
  A list of tables which define an argument comperator.
  
  NOTES:
   * All comperators must match for a rule to match
     (i.e. they are ANDed together).
   * You can compare an argument value only once per rule.
   
  See ``seccomp_rule_add_array(3)`` for details.

  arg
    The argument whose value is to be compared.
  
  op
    The operator to use when comparing the argument value with ``datum``.
    
    Supported operators:
    
     * ``ne``
     * ``lt``
     * ``le``
     * ``eq``
     * ``ge``
     * ``gt``
     * ``masked-eq``
    
    ``masked-eq`` works like ``eq`` after masking the argument value is masked
    with ``mask`` using a bit-wise AND.
    
  mask
    If ``op`` is ``masked-eq`` and only then a mask must be specified.
    This can either be an integer or string with a ``#define`` constant.
    
  datum
    The datum with which the argument value is compared. This can either be an
    integer or string with a ``#define`` constant.

add_exact
  Use ``seccomp_rule_add_exact_array`` rather than ``seccomp_rule_add_array``
  to add this rule. See ``seccomp_rule_add_exact_array(3)`` for details.

ASSERTIONS
==========

A table of ``#define`` constants and their values. Compiling this filter will
fail if any listed constant has a different value.

EXAMPLES
========

.. code-block:: toml

  name = "crabsecco-filter-example"
  description = "crabsecco-filter example"
  btree-optimization = false
  default-action = "Allow"
  architecture = "x86_64"

  [[rule]]
  action = "EPERM"
  syscall = "chroot"
  
  [[rule]]
  action = "EAFNOSUPPORT"
  syscall = "socket"
  comperators = [
    {arg = 0, op = "eq", datum = "AF_BLUETOOTH"},
  ]
  add_exact = true
  
  [assert]
  O_CREAT = 64
  

More examples can be found at
https://codeberg.org/crabjail/crabsecco/src/tag/v0.1.0/examples/.

SEE ALSO
========

``crabsecco(1)``
