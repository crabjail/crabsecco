#########
crabsecco
#########

crabjail seccomp compiler
#########################

:Version: 0.1.0
:Manual section: 1

SYNOPSIS
========

.. code-block::

  crabsecco [OPTIONS] <FILTER>

DESCRIPTION
===========

crabsecco compiles seccomp filter from toml files to bpf/pfc files. Read
``crabsecco-filter(5)`` for details on the format of the toml file.

OPTIONS
=======

``-h``, ``--help``
  Print help information

``--log-level <LOG_LEVEL>``
  Set the log level.

  [default: info] [possible values: off, error, warn, info, debug, trace]

``--log-location``
  Log the location in the source code where a message originates.

``--output-format <FORMAT>`` (or ``--of <FORMAT>``)
  Specify the output format. Multiple formats are allowed.
  
  [default: bpf] [possible values: bpf, pfc]

EXAMPLES
========

.. code-block::

  crabsecco examples/flatpak.toml
  
SEE ALSO
========

``crabsecco-filter(5)``
