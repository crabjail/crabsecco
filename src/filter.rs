/*
 * Copyright © 2022,2023 The crabsecco Authors
 *
 * This file is part of crabsecco
 *
 * crabsecco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabsecco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;

use anyhow::bail;
use anyhow::ensure;
use libseccomp::ScmpAction;
use libseccomp::ScmpArgCompare;
use libseccomp::ScmpCompareOp;
use libseccomp::ScmpFilterContext;
use libseccomp::ScmpSyscall;
use serde::Deserialize;
use serde::Deserializer;

use crate::get_define::get_define;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Filter {
    name: String,
    #[allow(dead_code)]
    description: String,
    btree_optimization: bool,
    #[serde(deserialize_with = "de_action")]
    default_action: ScmpAction,
    architecture: String,

    #[serde(rename = "rule")]
    rules: Vec<Rule>,

    #[serde(rename = "assert")]
    assertions: Option<Assertions>,
}
impl Filter {
    pub fn from_str(s: &str) -> Result<Self, toml::de::Error> {
        toml::from_str(s)
    }

    pub fn compile(&self) -> anyhow::Result<ScmpFilterContext> {
        if let Some(ref assertions) = self.assertions {
            assertions.check()?;
        }
        ensure!(std::env::consts::ARCH == self.architecture);

        let mut ctx = ScmpFilterContext::new_filter(self.default_action)?;

        // Log blocked syscalls
        ctx.set_ctl_log(true)?;

        // Set badarch action to Errno(EPERM) instead of KillThread
        ctx.set_act_badarch(ScmpAction::Errno(libc::EPERM))?;

        if self.btree_optimization {
            // Enable binary tree optimization
            ctx.set_ctl_optimize(2)?;
        }

        /* Prefetch all used constants with one run of cc.
        self.rules
            .iter()
            .flat_map(|rule| {
                rule.comperators
                    .iter()
                    .flat_map(|comperator| [comperator.mask, Some(comperator.datum)])
                    .filter_map(std::convert::identity)
                    .filter_map(|datum| {
                        if let U64OrStr::Str(s) = datum.datum {
                            Some(s)
                        } else {
                            None
                        }
                    })
            })
            .inspect(|x| { dbg!(x); })
            .collect::<HashSet<_>>();
        */

        let mut comperators = Vec::with_capacity(6);
        for rule in &self.rules {
            for comperator in &rule.comperators {
                let op = match (comperator.op.as_str(), comperator.mask.as_ref()) {
                    ("ne", None) => ScmpCompareOp::NotEqual,
                    ("lt", None) => ScmpCompareOp::Less,
                    ("le", None) => ScmpCompareOp::LessOrEqual,
                    ("eq", None) => ScmpCompareOp::Equal,
                    ("ge", None) => ScmpCompareOp::GreaterEqual,
                    ("gt", None) => ScmpCompareOp::Greater,
                    ("masked-eq", Some(mask)) => ScmpCompareOp::MaskedEqual(mask.to_sys()?),
                    ("masked-eq", None) => bail!("mask must be specified for masked-eq"),
                    (_, None) => bail!("Unknown op '{}'", comperator.op),
                    (_, Some(_)) => {
                        bail!("mask must not be specified for {}", comperator.op);
                    }
                };

                comperators.push(ScmpArgCompare::new(
                    comperator.arg,
                    op,
                    comperator.datum.to_sys()?,
                ));
            }
            if rule.add_exact {
                ctx.add_rule_conditional_exact(
                    rule.action,
                    ScmpSyscall::from_name(&rule.syscall)?,
                    &comperators,
                )?;
            } else {
                ctx.add_rule_conditional(
                    rule.action,
                    ScmpSyscall::from_name(&rule.syscall)?,
                    &comperators,
                )?;
            }
            comperators.clear();
        }

        Ok(ctx)
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
struct Rule {
    #[serde(deserialize_with = "de_action")]
    action: ScmpAction,
    syscall: String,
    #[serde(default)]
    comperators: Vec<Comperator>,
    #[serde(default)]
    add_exact: bool,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
struct Comperator {
    arg: u32,
    op: String,
    mask: Option<Datum>,
    datum: Datum,
}

#[derive(Debug, Deserialize)]
#[serde(transparent)]
struct Datum {
    datum: U64OrString,
}
impl Datum {
    fn to_sys(&self) -> anyhow::Result<u64> {
        match self.datum {
            U64OrString::U64(u) => Ok(u),
            U64OrString::String(ref s) => get_define(s),
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum U64OrString {
    U64(u64),
    String(String),
}

#[derive(Debug, Deserialize)]
#[serde(transparent)]
struct Assertions {
    assertions: HashMap<String, u64>,
}
impl Assertions {
    fn check(&self) -> anyhow::Result<()> {
        for (key, &val) in &self.assertions {
            ensure!(get_define(key)? == val);
        }

        Ok(())
    }
}

fn de_action<'de, D: Deserializer<'de>>(deserializer: D) -> Result<ScmpAction, D::Error> {
    let action = <String>::deserialize(deserializer)?;
    match action.as_str() {
        "Allow" => Ok(ScmpAction::Allow),
        "Deny" | "EPERM" => Ok(ScmpAction::Errno(libc::EPERM)),
        "Hide" | "ENOSYS" => Ok(ScmpAction::Errno(libc::ENOSYS)),
        "EAFNOSUPPORT" => Ok(ScmpAction::Errno(libc::EAFNOSUPPORT)),
        "kill" => Ok(ScmpAction::KillThread),
        _ => Err(serde::de::Error::custom(format!(
            "Unsupported action \"{action}\"",
        ))),
    }
}
