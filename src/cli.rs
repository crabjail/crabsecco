/*
 * Copyright © 2022,2023 The crabsecco Authors
 *
 * This file is part of crabsecco
 *
 * crabsecco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabsecco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::path::PathBuf;

use clap::Parser;

#[derive(Debug, Parser)]
#[command(about)]
pub struct Args {
    /// Set the log level
    #[arg(
        long,
        ignore_case = true,
        value_parser = clap::value_parser!(log::LevelFilter),
        default_value = "info",
    )]
    pub log_level: log::LevelFilter,

    /// Log the location in the source code where a message originates
    #[arg(long)]
    pub log_location: bool,

    /// Specify the output format. Multiple formats are allowed
    #[arg(
        long,
        visible_alias = "of",
        value_parser = ["bpf", "pfc"],
        default_value = "bpf",
        value_delimiter = ',',
    )]
    pub output_format: Vec<String>,

    /// Path to the filter
    pub filter: PathBuf,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Args::command().debug_assert();
    }
}
