/*
 * Copyright © 2022,2023 The crabsecco Authors
 *
 * This file is part of crabsecco
 *
 * crabsecco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabsecco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::io::prelude::*;
use std::process::Command;
use std::sync::Mutex;

use anyhow::anyhow;
use anyhow::bail;
use anyhow::ensure;
use once_cell::sync::Lazy;
use tempfile::NamedTempFile;

const INCLUDE_COMMON_HEADERS: &str = "\
#define _GNU_SOURCE
#include <fcntl.h>
#include <limits.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <sys/capability.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <sys/personality.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/select.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>
";

pub fn get_define(definename: &str) -> anyhow::Result<u64> {
    static DEFINE_CACHE: Lazy<Mutex<HashMap<String, u64>>> =
        Lazy::new(|| Mutex::new(HashMap::new()));

    let mut define_cache = DEFINE_CACHE.lock().map_err(|e| anyhow!("{e}"))?;

    if let Some(&val) = define_cache.get(definename) {
        return Ok(val);
    }

    let mut program = NamedTempFile::new()?;
    program.write_all(INCLUDE_COMMON_HEADERS.as_bytes())?;
    write!(
        program,
        "
        #ifndef {definename}
        # define {definename}
        #endif
        CRABSECCO_DEFINE__{definename} : {definename}
        ",
    )?;

    let output = Command::new("cc")
        .args(["-Wall", "-x", "c", "-E", "-P"])
        .arg(program.path())
        .output()?;
    ensure!(output.status.success());
    if !output.stderr.is_empty() {
        log::warn!("cc stderr:\n{}", String::from_utf8_lossy(&output.stderr));
    }

    let stdout = String::from_utf8(output.stdout)?;
    let int_literal = stdout
        .rsplit_once(':')
        .ok_or_else(|| anyhow!("get_define(definename='{definename}'): Parsing failed."))?
        .1
        .trim()
        .trim_matches(&['(', ')'][..])
        .trim_end_matches(&['u', 'U', 'l', 'L'][..])
        .to_lowercase();

    if int_literal.is_empty() {
        bail!("int_literal is empty for {definename}")
    }

    #[allow(clippy::from_str_radix_10)]
    let val = if let Some(hex_int_literal) = int_literal.strip_prefix("0x") {
        u64::from_str_radix(hex_int_literal, 16)?
    } else if let Some(bin_int_literal) = int_literal.strip_prefix("0b") {
        u64::from_str_radix(bin_int_literal, 2)?
    } else if int_literal.starts_with('0') {
        u64::from_str_radix(&int_literal, 8)?
    } else {
        u64::from_str_radix(&int_literal, 10)?
    };

    define_cache.insert(definename.to_string(), val);

    Ok(val)
}
