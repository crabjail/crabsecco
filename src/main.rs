/*
 * Copyright © 2022,2023 The crabsecco Authors
 *
 * This file is part of crabsecco
 *
 * crabsecco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabsecco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#![warn(rust_2018_idioms)]
#![deny(missing_debug_implementations)]
#![warn(clippy::clone_on_ref_ptr)]
#![warn(clippy::inefficient_to_string)]
#![warn(clippy::semicolon_if_nothing_returned)]
#![warn(clippy::string_to_string)]
#![warn(clippy::pattern_type_mismatch)]

//#[macro_use]
//mod utils;

mod cli;
mod filter;
mod get_define;

use std::fs;
use std::fs::File;

use clap::Parser;

fn main() -> anyhow::Result<()> {
    let args = cli::Args::parse();

    simplelog::TermLogger::init(
        args.log_level,
        simplelog::ConfigBuilder::new()
            .set_time_level(log::LevelFilter::Off)
            .set_thread_level(log::LevelFilter::Off)
            .set_target_level(log::LevelFilter::Error)
            .set_location_level(if args.log_location {
                log::LevelFilter::Error
            } else {
                log::LevelFilter::Off
            })
            .build(),
        simplelog::TerminalMode::Stderr,
        simplelog::ColorChoice::Auto,
    )
    .expect("Failed to init TermLogger");

    crabsecco(&args)
}

fn crabsecco(args: &cli::Args) -> anyhow::Result<()> {
    log::info!("Read '{}'.", args.filter.display());
    let raw_filter = fs::read_to_string(&args.filter)?;
    let filter = filter::Filter::from_str(&raw_filter)?;
    let ctx = filter.compile()?;

    let filter_name = filter.name();
    if args.output_format.iter().any(|of| of == "bpf") {
        log::info!("Export '{filter_name}' to '{filter_name}.bpf'.");
        let mut filter_bpf = File::create(filter_name.to_string() + ".bpf")?;
        ctx.export_bpf(&mut filter_bpf)?;
        filter_bpf.sync_all()?;
    }
    if args.output_format.iter().any(|of| of == "pfc") {
        log::info!("Export '{filter_name}' to '{filter_name}.pfc'.");
        let mut filter_pfc = File::create(filter_name.to_string() + ".pfc")?;
        ctx.export_pfc(&mut filter_pfc)?;
        filter_pfc.sync_all()?;
    }

    Ok(())
}
