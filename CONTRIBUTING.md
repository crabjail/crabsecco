# Contributing

**Table Of Contents**

- [Coding style](#coding-style)
  - [Rust](#rust)

## Coding style

## Rust

Run `cargo fmt` to format your code before committing. Keep in mind that this
will not format macros, you have to format them yourself.

Also run `cargo clippy` and fix all of the warnings/errors related to your code
or allow them if you believe that they are false positives. `cargo clippy`
should run without warnings/errors except for warnings/errors caused by
newer/older versions of clippy. Moreover you should look if
`cargo clippy -- --warn clippy::pedantic --warn clippy::nursery` reports
warnings/errors which seem to be correct.
